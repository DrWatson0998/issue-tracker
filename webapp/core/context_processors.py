from tracker.models import Status, Style


def statuses(request):
    return {
        'statuses': Status.objects.all()
    }

def styles(request):
    return {
        'styles': Style.objects.all()
    }