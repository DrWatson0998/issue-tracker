from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import Manager
from django.db.models.deletion import get_candidate_relations_to_delete


class CustomModelManager(Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_deleted=False)


class Entity(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Время создания")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Время изменения")
    is_deleted = models.BooleanField(default=False)

    objects = CustomModelManager()

    def delete(self, using=None, keep_parents=False):
        self.is_deleted = True
        self.save(update_fields=['is_deleted', ])

    class Meta:
        abstract = True


class Project(models.Model):
    start_date = models.DateField(blank=False, null=False, verbose_name="Дата начала")
    end_date = models.DateField(blank=True, null=True, verbose_name="Дата окончания")
    title = models.CharField(max_length=200, null=False, blank=False)
    description = models.TextField(max_length=3000, null=True, blank=True)
    is_deleted = models.BooleanField(default=False)
    user = models.ManyToManyField(
        get_user_model(), default=1,
        related_name='projects', verbose_name='Пользователь'
    )

    objects = CustomModelManager()

    def delete(self, using=None, keep_parents=False):
        self.is_deleted = True
        delete_candidates = get_candidate_relations_to_delete(self.__class__._meta)
        if delete_candidates:
            for rel in delete_candidates:
                if rel.on_delete.__name__ == 'CASCADE' and rel.one_to_many and not rel.hidden:
                    for item in getattr(self, rel.related_name).all():
                        item.delete()

        self.save(update_fields=['is_deleted', ])

    class Meta:
        permissions = [
            ('add_or_delete_member', 'Добавить/Удалить пользователя к проекту')
        ]


class Issue(Entity):
    summary = models.CharField(max_length=200, null=False, blank=False)
    description = models.TextField(max_length=3000, null=True, blank=True)
    status = models.ForeignKey('tracker.Status', on_delete=models.PROTECT, null=False, blank=False)
    style = models.ManyToManyField('tracker.Style', related_name='issues', blank=True)
    project = models.ForeignKey('tracker.Project', on_delete=models.CASCADE, related_name='issues', default=1, null=True, blank=True)


class Status(models.Model):
    NEW = 'Новый'
    IN_PROGRESS = 'В процессе'
    DONE = 'Выполнено'

    STATUS = (
        (NEW, NEW), (IN_PROGRESS, IN_PROGRESS), (DONE, DONE)
    )

    status = models.CharField(max_length=20, choices=STATUS, null=False, blank=False, default=NEW)


class Style(models.Model):
    TASK = 'Задача'
    BUG = 'Ошибка'
    ENHANCEMENT = 'Улучшение'

    STYLE = (
        (TASK, TASK), (BUG, BUG), (ENHANCEMENT, ENHANCEMENT)
    )

    style = models.CharField(max_length=20, choices=STYLE, null=False, blank=False, default=TASK)

