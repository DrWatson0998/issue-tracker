from django.contrib import admin
from .models import Issue, Status, Style, Project


# Register your models here.
class ProjectAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'start_date', 'end_date']
    list_filter = ['title']
    search_fields = ['title', 'start_date', 'end_date']
    fields = ['title', 'description', 'start_date', 'end_date']


class IssueAdmin(admin.ModelAdmin):
    list_display = ['id', 'summary', 'status', 'created_at']
    list_filter = ['status', 'style']
    search_fields = ['summary', 'status', 'status']
    fields = ['summary', 'description', 'status', 'style', 'project', 'created_at', 'updated_at']
    readonly_fields = ['created_at', 'updated_at']


class StatusAdmin(admin.ModelAdmin):
    list_display = ['id', 'status']
    search_fields = ['status']
    fields = ['status']


class StyleAdmin(admin.ModelAdmin):
    list_display = ['id', 'style']
    search_fields = ['style']
    fields = ['style']


admin.site.register(Project, ProjectAdmin)
admin.site.register(Issue, IssueAdmin)
admin.site.register(Status, StatusAdmin)
admin.site.register(Style, StyleAdmin)

