from django import forms
from tracker.models import Style, Status, Issue, Project
from tracker.validators import min_len_6, symbols_in_string


class IssueForm(forms.ModelForm):
    summary = forms.CharField(max_length=100, required=True, validators=(min_len_6, symbols_in_string))
    description = forms.CharField(max_length=3000, required=False)
    status = forms.ModelChoiceField(queryset=Status.objects.all())
    style = forms.ModelMultipleChoiceField(queryset=Style.objects.all())

    class Meta:
        model = Issue
        fields = '__all__'


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['title', 'description', 'start_date', 'end_date']


class ProjectMemberForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['user']

class SearchForm(forms.Form):
    search = forms.CharField(max_length=100, required=False)
