from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect, get_object_or_404, render
from django.urls import reverse

from tracker.forms import IssueForm, SearchForm
from tracker.helpers import SearchView
from tracker.models import Issue, Project
from django.views.generic import UpdateView, CreateView, ListView, DetailView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin, PermissionRequiredMixin


class IssueListView(SearchView):
    template_name = 'issue/list_issue_view.html'
    context_object_name = 'issues'
    model = Issue
    ordering = ('-created_at',)
    paginate_by = 6
    search_form = SearchForm
    search_fields = {
        'summary': 'icontains',
        'description': 'istartswith'
    }


class DetailIssueView(UserPassesTestMixin, DetailView):
    template_name = 'issue/detail_issue_view.html'
    model = Issue
    context_object_name = 'issue'
    extra_context = {'title': 'Задача'}
    permission_required = 'tracker.view_issue'

    def test_func(self):
        project_pk = self.get_object().project_id
        project = get_object_or_404(Project, pk=project_pk)
        return self.request.user in project.user.all() and self.request.user.has_perm(self.permission_required)


class DeleteIssueView(UserPassesTestMixin, DeleteView):
    model = Issue
    permission_required = 'tracker.delete_issue'

    def get_success_url(self):
        return reverse('detail_project', kwargs={'pk': self.object.project.pk})

    def test_func(self):
        project_pk = self.get_object().project_id
        project = get_object_or_404(Project, pk=project_pk)
        return self.request.user in project.user.all() and self.request.user.has_perm(self.permission_required)


class CreateIssueView(LoginRequiredMixin, CreateView):
    template_name = 'project/detail_project_view.html'
    model = Issue
    form_class = IssueForm

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        project = get_object_or_404(Project, pk=kwargs.get('pk'))
        if user not in project.user.all() and user.has_perm('tracker.add_issue'):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('detail_project', kwargs={'pk': self.kwargs.get('pk')})

    def post(self, request, *args, **kwargs):
        project_pk = kwargs.get('pk')
        project = get_object_or_404(Project, pk=project_pk)
        form = self.form_class(data=request.POST)
        if form.is_valid():
            style = form.cleaned_data.get('style')
            issue = Issue.objects.create(
                summary=form.cleaned_data.get('summary'),
                description=form.cleaned_data.get('description'),
                status=form.cleaned_data.get('status'),
                project=project
            )
            issue.style.set(style)
            return redirect(self.get_success_url())
        return render(request, self.template_name, context={
            'project': project,
            'form': form
        })


class UpdateIssueView(UserPassesTestMixin, UpdateView):
    template_name = 'issue/update_issue_view.html'
    form_class = IssueForm
    model = Issue
    permission_required = 'tracker.change_issue'

    def get_success_url(self):
        return reverse('detail_project', kwargs={'pk': self.object.project.pk})

    def test_func(self):
        project_pk = self.get_object().project_id
        project = get_object_or_404(Project, pk=project_pk)
        return self.request.user in project.user.all() and self.request.user.has_perm(self.permission_required)
