from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin, PermissionRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import redirect, get_object_or_404, render
from django.urls import reverse, reverse_lazy

from tracker.forms import ProjectForm, ProjectMemberForm
from tracker.models import Project
from django.views.generic import UpdateView, CreateView, ListView, DetailView, DeleteView, TemplateView


class ProjectListView(ListView):
    template_name = 'project/list_project_view.html'
    context_object_name = 'projects'
    model = Project


class DetailProjectView(DetailView):
    template_name = 'project/detail_project_view.html'
    model = Project
    context_object_name = 'project'
    extra_context = {'title': 'Проект'}


class CreateProjectView(PermissionRequiredMixin, CreateView):
    template_name = 'project/create_project_view.html'
    model = Project
    form_class = ProjectForm
    permission_required = 'tracker.add_project'

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        if form.is_valid():
            project = Project.objects.create(
                start_date=form.cleaned_data.get('start_date'),
                description=form.cleaned_data.get('description'),
                end_date=form.cleaned_data.get('end_date'),
                title=form.cleaned_data.get('title')
            )
            project.user.add(request.user)
            return redirect('list_projects')
        return render(request, self.template_name, context={
            'form': form
        })


class UpdateProjectView(PermissionRequiredMixin, UpdateView):
    template_name = 'project/update_project_view.html'
    form_class = ProjectForm
    model = Project
    permission_required = 'tracker.change_project'

    def get_success_url(self):
        return reverse('detail_project', kwargs={'pk': self.get_object().pk})


class DeleteProjectView(PermissionRequiredMixin, DeleteView):
    model = Project
    success_url = reverse_lazy('list_projects')
    permission_required = 'tracker.delete_project'


class ProjectMembersListView(ListView):
    template_name = 'project/list_project_members_view.html'
    model = Project

    def get(self, request, *args, **kwargs):
        project = get_object_or_404(Project, pk=kwargs.get('pk'))
        context = {
            'project': project,
            'users': project.user.all()
        }
        return render(request, template_name=self.template_name, context=context)


class ProjectUpdateMembersView(UserPassesTestMixin, UpdateView):
    model = Project
    template_name = 'project/project_update_members_view.html'
    form_class = ProjectMemberForm
    permission_required = 'tracker.add_or_delete_member'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['users'] = User.objects.all()
        return context

    def get_success_url(self):
        return reverse('project_members', kwargs={'pk': self.kwargs.get('pk')})

    def post(self, request, *args, **kwargs):
        project_pk = kwargs.get('pk')
        project = get_object_or_404(Project, pk=project_pk)
        form = self.form_class(data=request.POST)
        if form.is_valid():
            user = form.cleaned_data.get('user')
            project.user.set(user)
            return redirect(self.get_success_url())
        return render(request, self.template_name, context={
            'project': project,
            'form': form
        })

    def test_func(self):
        return (
                self.request.user in self.get_object().user.all() and
                self.request.user.has_perm(self.permission_required)
        )

