from django.urls import path
from tracker.views.issue_view import (
    IssueListView, DetailIssueView,
    DeleteIssueView, CreateIssueView,
    UpdateIssueView,)

from tracker.views.project_view import (
    ProjectListView, DetailProjectView,
    CreateProjectView, UpdateProjectView,
    DeleteProjectView, ProjectMembersListView,
    ProjectUpdateMembersView
)

urlpatterns = []

issue_urls = [
    path('issues', IssueListView.as_view(), name='list_issues'),
    path('project/issue/<int:pk>/', DetailIssueView.as_view(), name='detail_issue'),
    path('project/issue/<int:pk>/delete/', DeleteIssueView.as_view(), name='delete_issue'),
    path('project/<int:pk>/issue/create', CreateIssueView.as_view(), name='create_issue'),
    path('project/issue/<int:pk>/update', UpdateIssueView.as_view(), name='update_issue'),
]

project_urls = [
    path('', ProjectListView.as_view(), name='list_projects'),
    path('project/<int:pk>/', DetailProjectView.as_view(), name='detail_project'),
    path('project/create', CreateProjectView.as_view(), name='create_project'),
    path('project/<int:pk>/update', UpdateProjectView.as_view(), name='update_project'),
    path('project/<int:pk>/delete', DeleteProjectView.as_view(), name='delete_project'),
    path('project/<int:pk>/members', ProjectMembersListView.as_view(), name='project_members'),
    path('project/<int:pk>/members/update', ProjectUpdateMembersView.as_view(), name='update_members'),
]

urlpatterns += issue_urls
urlpatterns += project_urls
