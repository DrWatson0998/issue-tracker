from django.core.exceptions import ValidationError


def min_len_6(string):
    if len(string) < 6:
        raise ValidationError('Количество символов должно быть больше 6')


def symbols_in_string(string):
    forbidden_characters = '!@?%$&'
    for symbol in forbidden_characters:
        if symbol in string:
            raise ValidationError('Вы используете запрещенные символы')