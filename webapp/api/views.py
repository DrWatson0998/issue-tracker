import json

from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from api.serializers import ProjectSerializer, IssueSerializer
from tracker.models import Project, Issue


class ProjectDetailView(APIView):
    def get(self, request, *args, **kwargs):
        project = get_object_or_404(Project, pk=kwargs.get('pk'))
        serializer = ProjectSerializer(project, many=False)
        return JsonResponse(serializer.data, safe=False)

    def put(self, request, *args, **kwargs):
        project = get_object_or_404(Project, pk=kwargs.get('pk'))
        data = json.loads(request.body)
        serializer = ProjectSerializer(project, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, safe=False)
        else:
            return Response({'errors': serializer.errors}, status=400)

    def delete(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        project = get_object_or_404(Project, pk=pk)
        project.delete()
        return Response({"pk": pk}, status=204)


class IssueDetailView(APIView):
    def get(self, request, *args, **kwargs):
        issue = get_object_or_404(Issue, pk=kwargs.get('pk'))
        serializer = IssueSerializer(issue, many=False)
        return JsonResponse(serializer.data, safe=False)

    def put(self, request, *args, **kwargs):
        issue = get_object_or_404(Issue, pk=kwargs.get('pk'))
        data = json.loads(request.body)
        serializer = IssueSerializer(issue, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, safe=False)
        else:
            return Response({'errors': serializer.errors}, status=400)

    def delete(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        issue = get_object_or_404(Issue, pk=pk)
        issue.delete()
        return Response({"pk": pk}, status=204)
