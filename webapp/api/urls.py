from django.urls import path

from api.views import (
    ProjectDetailView, IssueDetailView
)

urlpatterns = []

api_urls = [
    path('project/<int:pk>', ProjectDetailView.as_view()),
    path('issue/<int:pk>', IssueDetailView.as_view())
]

urlpatterns += api_urls
