from rest_framework import serializers

from tracker.models import Project, Issue


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ['id', 'start_date', 'end_date', 'title', 'description', 'is_deleted', 'user']
        read_only_fields = ['id']


class IssueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Issue
        fields = ['id', 'summary', 'description', 'status', 'style', 'project']
        read_only_fields = ['id']
