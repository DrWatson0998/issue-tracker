from django.contrib.auth import get_user_model
from django.db import models


class Profile(models.Model):
    user = models.OneToOneField(
        get_user_model(), related_name='profile',
        on_delete=models.CASCADE, verbose_name='Пользователь'
    )
    image = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name='Фото пользователя')
    github_link = models.CharField(max_length=100, null=True, blank=True, verbose_name='Ссылка на GitHub')
    about_user = models.TextField(max_length=3000, null=True, blank=True, verbose_name='О себе')

    def __str__(self):
        return self.user.get_full_name() + "'s Profile"

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'
        permissions = [
            ('view_users_list', 'Просмотреть список пользователей')
        ]
